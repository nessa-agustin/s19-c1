// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:

let gradeResult = (grade) => {

    let mark;

    if(grade < 75){
        mark = "Failed";
    }else if(grade < 81){
        mark = "Beginner";
    }else if(grade < 86){
        mark = "Developing";
    }else if(grade < 91){
        mark = "Above Average";
    }else if(grade > 90){
        mark = "Advanced";
    }

    return `Your quarterly average is ${grade}. You have received a ${mark} mark.`
}

console.log(gradeResult(74));
console.log(gradeResult(76));
console.log(gradeResult(85));
console.log(gradeResult(92));
console.log(gradeResult(90));
console.log(gradeResult(95));




/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:

function oddEvenChecker(){

    for(let i = 1 ; i < 300 ; i++){

        let isEven = (i % 2) == 0 ? true : false;

        if(isEven){
            console.log(i + ' - even')
        }else{
            console.log(i + ' - odd')
        }
    }
}

oddEvenChecker();



/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

let hero = {
    heroName: '',
    origin: '',
    description: '',
    skills: {
        skill1: '',
        skill2: '',
        skill3: ''
    }
};

hero.heroName = prompt("Enter Hero Name:");
hero.origin = prompt("Enter Hero Origin:");
hero.description = prompt("Enter Hero Description:");
hero.skills.skill1 = prompt("Enter Hero Skill 1:");
hero.skills.skill2 = prompt("Enter Hero Skill 2:");
hero.skills.skill3 = prompt("Enter Hero Skill 3:");

console.log(JSON.stringify(hero));



